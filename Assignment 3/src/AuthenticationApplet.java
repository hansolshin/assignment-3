import java.awt.*;
import java.awt.event.*;
import java.applet.Applet;


public class AuthenticationApplet extends Applet{
   /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
Button write = new Button("Authenticate");
   Label label1 = new Label("Enter UserName");
   TextField text = new TextField(15);
   Label label2 = new Label("Enter Password");
   TextField text2 = new TextField(15);
   public void init(){
      add(label1);
      label1.setBackground(Color.lightGray);
      add(text);
      add(label2);
      label2.setBackground(Color.lightGray);
      add(text2);
      add(write,BorderLayout.CENTER);
      write.addActionListener(new ActionListener (){
        public void actionPerformed(ActionEvent e){
         new Authenticating();
        }
     }
   );
}
}